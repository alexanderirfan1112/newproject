import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Login from './login';
import Biodata from './Biodata';
export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor='background: #0ACBF5;'
          barstyle='light-content'
          />
          <Biodata/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#56CCF2',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

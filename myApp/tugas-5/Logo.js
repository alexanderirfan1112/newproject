import React, {Component} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style = {styles.container}>
                    <Image styles = {{width:4, height: 7}}
                    source = {require('./assets/stt.png')}/>
                    
                    <Text style={styles.logoText}>L O G I N</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 1,
          justifyContent: 'flex-end',
          alignItems: 'center'
        }, 
    logoText: {
        marginVertical: 51,
        fontSize:20,
        color: 'rgba(0, 0, 0, 0.75)'
    }
});
